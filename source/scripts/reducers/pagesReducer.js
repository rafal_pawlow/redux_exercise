export default function reducer(state={
	page: 1,
	totalElements: localStorage.getItem('elementsOnPage') || 10
}, action) {

	switch (action.type) {
		case "SWITCH_PAGE": {
			return Object.assign({}, state, {page: action.page});
		}
		case "SET_ELEMENTS": {
			return Object.assign({}, state, {totalElements: action.totalElements, page: action.page});
		}
	}

	return state;
}