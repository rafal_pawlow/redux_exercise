export default function reducer(state={
	posts: [],
	fetching: false,
	fetched: false,
}, action) {

	switch (action.type) {
		case "FETCH_POSTS": {
			return Object.assign({}, state, {fetching: true});
		}
		case "RECEIVE_POSTS": {
			return Object.assign({}, state, {fetching: false, fetched: true, posts: action.payload});
		}
	}

	return state;
}