import { combineReducers } from 'redux';

import users from './userReducer';
import page from './pagesReducer';
import posts from './postsReducer';

export default combineReducers({
	users,
	page,
	posts
})