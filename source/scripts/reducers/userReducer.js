export default function reducer(state={
	users: [],
	fetching: false,
	fetched: false,
}, action) {

	switch (action.type) {
		case "FETCH_USERS": {
			return Object.assign({}, state, {fetching: true});
		}
		case "RECEIVE_USERS": {
			return Object.assign({}, state, {fetching: false, fetched: true, users: action.payload});
		}
	}

	return state;
}