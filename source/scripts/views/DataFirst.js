import React from 'react';

import { connect } from 'react-redux';
import { fetchPosts } from '../actions/postsActions';
import { switchPage, setTotalElements } from '../actions/pagesActions';

import Pagination from '../layout/Pagination';

@connect(store => {
	return {
		posts: store.posts.posts,
		isLoading: store.posts.fetched,
		page: store.page.page,
		totalElements: store.page.totalElements
	};
})

export default class DataFirst extends React.Component {
	componentWillMount() {
		this.props.dispatch(fetchPosts());
	}

	componentDidMount() {
		document.getElementsByTagName('select')[0].value = this.props.totalElements;
	}

	getBeginningIndex() {
		return (this.props.page - 1) * this.props.totalElements;
	}

	getEndingIndex() {
		return Math.min(this.props.page * this.props.totalElements, this.props.posts.length); 
	}

	visibleElements(e) {
		this.props.dispatch(setTotalElements(e.target.value))
	}

	render() {
		let child = [],
			results = 20;

		window.scrollTo(0,0)
		this.props.posts.slice(this.getBeginningIndex(),this.getEndingIndex()).map((data,key) => {
			child.push(
				<tr key={key}>
					<td>{data.userId}</td>
					<td>{data.title}</td>
					<td>{data.body}</td>
					<td>{key}</td>
				</tr>)
		})
		return(
			<div class="data">
				<div class={(!this.props.isLoading ? "is-visible" : "") + " spinner"}>
					<span>Loading...</span>
				</div>
				<label>
					<span>Ilość elementów na stronie: </span>
					<select onChange={(e) => this.visibleElements(e)}>
						<option>10</option>
						<option>20</option>
						<option>30</option>
						<option>40</option>
						<option>50</option>
						<option>60</option>
						<option>70</option>
					</select>
				</label>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>User ID</th>
							<th>Title</th>
							<th>Post</th>
							<th>#</th>
						</tr>
					</thead>
					<tbody>
						{child}
					</tbody>
				</table>
				<Pagination arrLength={this.props.posts.length}/>
			</div>
		);
	}
}