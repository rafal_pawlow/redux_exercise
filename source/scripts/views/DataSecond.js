import React from 'react';

import { connect } from 'react-redux';
import { fetchUsers } from '../actions/userActions';

@connect(store => {
	return {
		users: store.users.users,
		isLoading: store.users.fetched
	};
})

export default class DataSecond extends React.Component {
	componentWillMount() {
		this.props.dispatch(fetchUsers());
	}

	render() {
		let child = [];
		this.props.users.map((data,key) => {
			child.push(
				<div key={key} class="col-xs-6 col-sm-4 col-lg-3 single-user">
					<img src="http://lorempixel.com/300/300/people/"/>
					<div class="single-user-data-wrapper">
						<span>Full name</span>
						<span>{data.name}</span>
					</div>
					<div class="single-user-data-wrapper">
						<span>Username</span>
						<span>{data.username}</span>
					</div>
					<div class="single-user-data-wrapper">
						<span>Email</span>
						<span>{data.email}</span>
					</div>
				</div>)
		});

		return(
			<div class="users-container">
				<div class={(!this.props.isLoading ? "is-visible" : "") + " spinner"}>
					<span>Loading...</span>
				</div>
				{child}
			</div>
		);
	}
}