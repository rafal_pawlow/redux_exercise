import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import { Router, Route, IndexRoute, browserHistory, useRouterHistory } from 'react-router';
import { createHistory } from 'history';

import Layout from './layout/Layout';
import Home from './views/Home';
import Posts from './views/DataFirst';
import Users from './views/DataSecond';
import store from './store';

const history = useRouterHistory(createHistory)();
const app = document.getElementById('base');

ReactDOM.render(
	<Provider store={store}>
		<Router onUpdate={() => window.scrollTo(0, 0)} history={history}>
			<Route path='/' component={Layout}>
				<IndexRoute component={Home}></IndexRoute>
				<Route path="posts" component={Posts}></Route>
				<Route path="users" component={Users}></Route>
			</Route>
		</Router>
	</Provider>
,app);