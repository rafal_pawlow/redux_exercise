import ApiClient from '../utilities/ApiClient';

export function fetchPosts() {
	return dispatch => {
		dispatch({type: 'FETCH_POSTS'});
		ApiClient.apiConnector('posts').then(response => {
			return response.json();
		}).then(response => {
			dispatch({type: 'RECEIVE_POSTS', payload: response});
		}).catch(error => {
    		alert(error);
  		});
	}
}