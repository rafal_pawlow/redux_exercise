export function switchPage(page) {
	return dispatch => {
		dispatch({type: 'SWITCH_PAGE', page: page});
	}
}

export function setTotalElements(elements) {
	return dispatch => {
		localStorage.setItem('elementsOnPage',elements);
		dispatch({type: 'SET_ELEMENTS', totalElements: elements, page: 1})
	}
}