import ApiClient from '../utilities/ApiClient';

export function fetchUsers() {
	return dispatch => {
		dispatch({type: 'FETCH_USERS'});
		ApiClient.apiConnector('users').then(response => {
			return response.json();
		}).then(response => {
			dispatch({type: 'RECEIVE_USERS', payload: response});
		}).catch(error => {
    		alert(error);
  		});
	}
}