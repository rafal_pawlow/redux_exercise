import React from 'react';

import { Link } from 'react-router';

export default class Header extends React.Component {

	toggleSidebar() {
		let isOpen = controlSideBar();
		this.setState({isOpen: isOpen})
	}

	render() {
		return(
			<header>
				<button class="hamburger-button" onClick={this.props.determineSidebarState}></button>
				<Link to="/"><h2>Xsolve - excercise</h2></Link>
			</header>
		);
	}
}

Header.propTypes = {
	determineSidebarState: React.PropTypes.func
};