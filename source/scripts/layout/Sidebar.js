import React from 'react';

import NavLink from './NavLink';

export default class Sidebar extends React.Component {
	constructor() {
		super();
	}

	render() {
		return(
			<nav class={(this.props.isOpen ? "is-open" : "" ) + " layout-navbar"}>
				<ul class="nav nav-pills nav-stacked" id="menu">
					<NavLink onClick={this.props.determineSidebarState} to="/" activeClassName="active" onlyActiveOnIndex={true}>
						<span>Home</span>
					</NavLink>
					<NavLink onClick={this.props.determineSidebarState} to="posts" activeClassName="active">
						<span>Posts</span>
					</NavLink>
					<NavLink onClick={this.props.determineSidebarState} to="users" activeClassName="active">
						<span>Users</span>
					</NavLink>
				</ul>
				<div onClick={this.props.determineSidebarState} class="layout-overlay"></div>
			</nav>
		);
	}
}

Sidebar.propTypes = {
	determineSidebarState: React.PropTypes.func
};