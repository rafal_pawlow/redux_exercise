import React from 'react';

import { connect } from 'react-redux';
import { switchPage, setTotalElements } from '../actions/pagesActions';

@connect(store => {
	return {
		page: store.page.page,
		totalElements: store.page.totalElements
	};
})

export default class Pagination extends React.Component {
	nextPage() {
		if (!this.isLast()) {
			this.selectPage(this.props.page + 1);
		}
	}

	prevPage() {
		if (!this.isFirst()) {
			this.selectPage(this.props.page - 1)
		}
	}

	isFirst() {
		return this.props.page < 2;
	}

	isLast() {
		return this.props.page >= this.props.totalElements;
	}

	selectPage(page) {
		this.props.dispatch(switchPage(page));
	}

	totalPages() {
  		return Math.ceil(this.props.arrLength / this.props.totalElements);
	}

	render() {
		let pages = [];

		for(let i = 1; i <= this.totalPages(); i++) {
			let active = this.props.page === i ? 'active' : '';
			pages.push(
				<li class={active} key={i}>
					<a onClick={() => this.selectPage(i)}>{i}</a>
				</li>
			);
		}

		if(pages < 2) {
			return null;
		}
		else {
			return(
				<nav class="navigation-container navigation-container--centered" aria-label="Page navigation">
					<ul class="pagination">
						<li class={this.isFirst()? "hidden" : ""} onClick={this.prevPage.bind(this)}>
							<a  aria-label="Previous">
							<span aria-hidden="true">&laquo;</span>
							</a>
						</li>
						{pages}
						<li class={this.isLast() ? "hidden" : ""} onClick={this.nextPage.bind(this)}>
							<a  aria-label="Next">
								<span aria-hidden="true">&raquo;</span>
							</a>
						</li>
					</ul>
				</nav>
			);
		}
	}
}