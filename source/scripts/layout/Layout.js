import React from 'react';

import Header from './Header';
import Sidebar from './Sidebar';

import { controlSideBar } from '../utilities/LayoutUtilities';

import { connect } from 'react-redux';

@connect(store => {
	return {
		users: store.users
	};
})

export default class Layout extends React.Component {
	constructor() {
		super();

		this.state = {
			isSidebarOpen: false
		}
	}

	handleHamburgerClick() {
		this.setState({isSidebarOpen: controlSideBar()});
	}

	render() {
		return(
			<div>
				<Header determineSidebarState={this.handleHamburgerClick.bind(this)}/>
				<div class={(this.state.isSidebarOpen ? "layout-fixed" : "") + " layout-wrapper"}>
					<Sidebar determineSidebarState={this.handleHamburgerClick.bind(this)} isOpen={this.state.isSidebarOpen}/>
					<div class="container">
						{this.props.children}
					</div>
				</div>
			</div>
		);
	}
}