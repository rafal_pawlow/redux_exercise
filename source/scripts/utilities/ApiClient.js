class ApiClient {
	constructor() {
		this.apiKey = 'https://jsonplaceholder.typicode.com/';
	}

	apiConnector(endpoint,method) {
		return new Promise((resolve, reject) => {
			return fetch(this.apiKey + endpoint, {
				method: method || 'GET',
				mode: 'cors',
				cache: 'default',
				headers: {
					'Accept': 'application/json',
           			'Content-Type': 'application/json'
				}
			})
			.then(this._handleErrors)
			.then(response => resolve(response))
			.catch(error => reject(error));
		});
	}

	_handleErrors(response) {
		if(!response.ok) {
			throw new Error("HTTP error: " + response.status + "  Details: " + response.statusText);
		}
		return response;
	}
}

export default (new ApiClient);