let isSidebarOpen = false;

export function controlSideBar() {
	switch(isSidebarOpen) {
	case false:
		isSidebarOpen = true;
		break;
	case true:
		isSidebarOpen = false
		break;
	}
	return isSidebarOpen;
}

export function getSidebarState() {
	return isSidebarOpen;
}