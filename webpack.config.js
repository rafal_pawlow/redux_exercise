const	webpack = require('webpack'),
		path = require('path'),
		ManifestPlugin = require('webpack-manifest-plugin'),
		ChunkManifestPlugin = require('chunk-manifest-webpack-plugin'),
		ExtractTextPlugin = require("extract-text-webpack-plugin"),
		CleanWebpackPlugin = require('clean-webpack-plugin'),
		ProgressBarPlugin = require('progress-bar-webpack-plugin'),
		HtmlWebpackPlugin = require('html-webpack-plugin');

var		debug = process.env.NODE_ENV !== 'production';

module.exports = {
	context: path.join(__dirname + '/source'),
	devtool: debug ? 'source-map' : 'cheap-module-source-map',
	entry: [
		'es6-promise',
		'whatwg-fetch',
		'./scripts/app.js',
		'./public/styles/codebase.scss'
	],
	output: {
		path: path.join(__dirname, '/deploy'),
		filename: '/js/[name].[hash].js',
		sourceMapFileName: '[file].map'
	},
	resolve: {
		modulesDirectories: ['node_modules', 'scripts'],
		extensions: ['', '.js']
	},
	module: {
		loaders: [
			{
				test: /\.js?$/,
				exclude: /(node_modules)/,
				loader: 'babel-loader',
				query: {
					presets: ['react', 'es2015'],
					plugins: ['react-html-attrs', 'transform-decorators-legacy', 'transform-class-properties']
				}
			},
			{
				test: /\.scss$/,
				loader: ExtractTextPlugin.extract('style-loader', 'css-loader?mimetype=image/css&sourceMap!autoprefixer-loader?{browsers:["last 10 chrome versions","last 10 firefox versions","last 10 opera versions","last 10 safari versions","iOS >= 7","android >= 4.0","ie >= 9"]}!sass-loader?sourceMap'),
			},
			{ 
				test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, 
				loader: "file-loader?&mimetype=application/font-woff&name=/fonts/[name].[ext]" },
			{ 	
				test: /\.(ttf|eot)(\?v=[0-9]\.[0-9]\.[0-9])?$/, 
				loader: "file-loader?name=./fonts/[name].[ext]" 
			},
			{
				test: /\.svg/, 
				loader: 'svg-url-loader'
			},
			{ 
				test: /\.(gif|jpg)?$/, 
				loader: "file-loader?name=/graphics/[name].[ext]" 
			}
		]
	},
	sassLoader: {
		includePaths: ['/public/styles', 'node_modules/bootstrap_sass/assets/stylesheets']
	},
	plugins: debug ? [
		new webpack.DefinePlugin({
		    PRODUCTION: JSON.stringify(false)
		}),
		new HtmlWebpackPlugin({
			filename: 'index.html',
			template: path.join(__dirname + '/source/scripts/templates/index-template.ejs')
		}),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.optimize.OccurenceOrderPlugin(),
		new webpack.optimize.DedupePlugin(),
		new CleanWebpackPlugin(['deploy'], {
			root: __dirname,
			verbose: true,
			dry: false
		}),
		new ProgressBarPlugin({
			format: ':percent [:current/:total] :msg \n',
			clear: false
		}),
		new ExtractTextPlugin('/css/[name].[hash].css')
	] : [
		new webpack.DefinePlugin({
		    PRODUCTION: JSON.stringify(true),
	      	'process.env.NODE_ENV': `"${process.env.NODE_ENV}"`
		}),
		new ExtractTextPlugin('/css/[name].[hash].css'),
		new CleanWebpackPlugin(['deploy'], {
			root: __dirname,
			verbose: true,
			dry: false
		}),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.optimize.OccurenceOrderPlugin(),
		new webpack.optimize.DedupePlugin(),
		new webpack.optimize.CommonsChunkPlugin({
			name: 'vendor',
			minChunks: Infinity
		}),
		new ChunkManifestPlugin({
			filename: 'chunk-manifest.json',
			manifestVariable: 'webpackManifest'
		}),
		new ManifestPlugin(),
		new ProgressBarPlugin({
			width: 80,
			clear: false
		}),
		new HtmlWebpackPlugin({
			filename: 'index.html',
			template: path.join(__dirname + '/source/scripts/templates/index-template.ejs')
		}),
	]

}