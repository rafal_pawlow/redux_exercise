#Redux Exercise - Readme
####Dependencies installation####

* make sure you use at least node **6.6.0** (for better expercience install **NVM**)
* run `npm install` in project's root
* run `npm install` in /source directory

####Development command (webpack hot server)####
`npm start`

####Production build command####
`npm run build:production`

####Development build command####
`npm run build:development`